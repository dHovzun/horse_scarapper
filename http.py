#!/usr/bin/python3

import requests
import pendulum

from logger import SimpleLogger
from resources import resources


class Http(SimpleLogger):
    url = None

    def __init__(self, url):
        super().__init__()
        self.url = url

    def make_get(self, **query):
        response = requests.get(self.url, params=query)

        return self.__handle_response(response)

    def make_post(self, **data):
        response = requests.post(self.url, data=data)

        return self.__handle_response(response)

    def __handle_response(self, response):
        if response.ok:
            return response.content
        else:
            now = pendulum.now().format('Y-m-d H:i:s')
            print(
                resources['logger_messages']['bad_response'].format(now, response.status_code, self.url)
            )

            return ''
