#!/usr/bin/python3

from lxml import html
from bs4 import BeautifulSoup


class Parser:
    bs_object = None
    lxml_object = None

    def __init__(self, content):
        self.bs_object = BeautifulSoup(content, 'html.parser')
        self.lxml_object = html.fromstring(content)

    def parse_horses(self):
        horses_list = self.bs_object.find_all('search-resultList__item')

    def format_horses_data(self):
        pass
