#!/usr/bin/python

resources = {
    'search_horse_url': 'https://www.racingpost.com/search/',
    'search_horse_parent_url': 'https://www.pedigreequery.com/',
    'messages': {
        'invalid': 'Some error has happened! Please check error.log file.',
        'invalid_args': 'Wrong args!\n',
        'help': 'Command arguments:\n'
                + '1. -p|page= start page for searching horse\n'
                + '2. -e|save-to-excel= how many objects need to parse before saving it to output.xlsx'
    },
    'logger_messages': {
        'cant_parse_args': '{0}: Can\'t parse arguments!\nMessage: {1}',
        'bad_response': '{0}: Error with code={1} has happened when trying to get content from url="{2}"',
    },
    'default_values': {
        'page': 1,
        'save_to_excel': 10
    },
}
