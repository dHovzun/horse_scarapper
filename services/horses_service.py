import itertools
import math

import numpy as np
from http import Http

from resources import resources


class HorsesService:
    horse_on_one_page = 10

    def __init__(self):
        self.horse_client = Http(resources['search_horse_url'])
        self.horse_parents_client = Http(resources['search_horse_parent_url'])

    def parse_horses(self, horses_count, start_page):
        horses = ()
        pages = int(self.calculate_pages_count(horses_count))
        for page_number in range(start_page, start_page + pages):
            response = self.horse_client.make_get(**{'page': page_number})
            if response:  # todo: тут нужно вызывать парсер, он возвращает уже словарь
                pass

    def calculate_pages_count(self, how_many_searching):
        return math.ceil(how_many_searching / self.horse_on_one_page)

    @staticmethod
    def __generate_names(sections):
        chars = [chr(c) for c in range(ord('a'), ord('z') + 1)]
        keywords = [''.join(i) for i in itertools.product(chars, repeat=4)]

        return np.array_split(keywords, sections)
