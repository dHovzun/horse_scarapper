import logging


class SimpleLogger:
    error_log_name = 'errors.log'
    info_log_name = 'info.log'

    error_log = None
    info_log = None

    def __init__(self):
        self.__prepare_logging_files()
        self.error_log = logging.getLogger(self.error_log_name)
        self.info_log = logging.getLogger(self.info_log_name)

    def __prepare_logging_files(self):
        if logging.getLogger(self.error_log_name) is None:
            logging.basicConfig(filename=self.error_log_name, filemode='w')

        if logging.getLogger(self.info_log_name) is None:
            logging.basicConfig(filename=self.info_log_name, level=logging.INFO, filemode='w')
