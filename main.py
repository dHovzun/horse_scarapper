#!/usr/bin/python3
"""Отсюда будет  осуществляться запуск"""
import getopt
import sys
from pendulum import now

from logger import SimpleLogger
from resources import resources


# todo: как будет работать скрипт
# todo: 1. Его запускают из консоли
# todo: 2. Метод хэндлит аргументы и формирует параметры для запроса
# todo:     2.1. Аргументы -- вот тут еще подумать, скорее всего нужно будет задавать количество объектов для записи
# todo:          в эксель, имя  лошади и страницу для поиска
# todo: 3. Результатом запроса будет content (при поиске родственников на педигри нужно помнить
#          про 404 ошибку при отсуствии родственников)
# todo: 4. Дальше этот контент отправляется в парсер, который  возвращает словари
# todo: 5. Происходит запись в excel файл
# todo: 6. Обязательно нужно добавить логирование хотя бы в 1 файл для  следующих случаев:
# todo:    1. Все штуки связанные с запросами в интернет
# todo:    1. Все штуки связанные парсингом
# todo:    Подробнее о них позже

class ScriptHandler(SimpleLogger):
    errors_count = 0

    def __init__(self):
        super().__init__()

    def handle(self, argv):
        args = self.__parse_argv(argv[1:])

        handled_args = self.__handle_arguments(args)
        request_data = resources['default_values'] if not handled_args else handled_args

        if 'message' in request_data.keys():
            print(handled_args['message'])
            sys.exit(0)

        # data_to_save = Данные для записи в эксель, думаю сразу  надо будет из сервиса отправлять в нужном  формате

    @staticmethod
    def __handle_arguments(argv):
        result = {}
        for opt, arg in argv[0]:
            if opt in ('-p', "--page"):
                result['page'] = int(arg)
            elif opt in ("-e", "--save-to-excel"):
                result['save_to_excel'] = int(arg)
            elif opt in ('-h', '--help'):
                result['message'] = resources['messages']['help']
            else:
                result['message'] = resources['messages']['invalid_args'] + resources['messages']['help']

        return result

    @staticmethod
    def __parse_argv(argv):
        try:
            return getopt.getopt(argv, 'hp:e:', ['help', 'page=', 'save-to-excel='])
        except getopt.GetoptError as exception:
            print(
                resources['logger_messages']['cant_parse_args'].format(now().to_datetime_string(), str(exception))
            )
            sys.exit(0)


if __name__ == "__main__":
    script_handler = ScriptHandler()
    script_handler.handle(sys.argv)
